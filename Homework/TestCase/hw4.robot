*** Settings ***
Library         SeleniumLibrary

*** Variables ***
${BROWSER}      chrome
${URL}          http://www.doppio-tech.com:8080/login
${UserA}        dummy
${PassA}        dummy
${UserB}        dummy1
${PassB}        dummy1

*** Keywords ***


*** Test Cases ***
Doppio If-Else
    Open BROWSER                    ${URL}  ${BROWSER}
    Maximize Browser Window
    Input Text                      xpath://*[@name="j_username"]       ${UserB}
    Input PASSWORD                  xpath://*[@name="j_password"]       ${PassB}
    Sleep                           3s
    Click button                    css:input.submit-button.primary
    Element Text Should Be          css:div.alert.alert-danger          Invalid username or password
    #Need to have verify first before to to if else?

    ${Alert}  Get Text  css:div.alert.alert-danger

    IF  "${Alert}" == "Invalid username or password"
        Sleep                           3s
        Input Text                      xpath://*[@name="j_username"]       ${UserA}
        Input PASSWORD                  xpath://*[@name="j_password"]       ${PassA}
        Sleep                           3s
        Click button                    css:input.submit-button.primary
        Sleep                           3s
        Log to console  This is correct username & password. You are so COOL! Go to last test step!
        Close Browser
    ELSE
        Log to console  This is incorrect username & password. Sorry you are lost! Try again ^^
    END