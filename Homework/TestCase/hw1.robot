*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${BROWSER}  chrome
${URL}  http://www.doppiotech.com/
# ${CALL} (+66) 2 163 4570

*** Keywords ***




*** Test Cases ***
Doppiotech_homepage
    Open BROWSER    ${URL}  ${BROWSER}
    Maximize Browser Window
    Click Link      link:CONTACT US
    Scroll Element Into View    css:h4.elementor-heading-title.elementor-size-default
    Element Text Should Be    css:div.elementor-204 .elementor-element.elementor-element-7b025d0b .elementskit-infobox .box-body > p    (+66) 2 163 4570
    Sleep   3s
    Close Browser