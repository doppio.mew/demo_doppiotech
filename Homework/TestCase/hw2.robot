*** Settings ***
Library         SeleniumLibrary
Resource        ../Resource/DDT.robot
Library         DataDriver  ../TestData/setB.xlsx
Test Template   Login ผิดพลาดเเสดง error
# Suite Teardown  Close Browser

*** Variables ***
${BROWSER}  chrome
${URL}      http://www.doppio-tech.com:8080/login

*** Keywords ***
Login ผิดพลาดเเสดง error
    [Arguments]     ${USER}   ${PASSWORD}   ${ERROR_MESSAGE}
    Open BROWSER                    ${URL}  ${BROWSER}
    Maximize Browser Window
    Input Text                      xpath://*[@name="j_username"]       ${USER}
    Input PASSWORD                  xpath://*[@name="j_password"]       ${PASSWORD}
    Sleep                           3s
    Click button                    css:input.submit-button.primary
    Element Text Should Be          css:h1                              ${ERROR_MESSAGE}
    Sleep                           3s
    Close Browser


*** Test Cases ***
#Login ผิดพลาดเเสดง error     dummy      dummy           Access Denied
#Login ผิดพลาดเเสดง error     dummy1     dummy1          Welcome to Jenkins!
#Login ผิดพลาดเเสดง error      ${USER}    ${PASSWORD}     ${ERROR_MESSAGE}

Login ผิดพลาดเเสดง error using ${USER} and ${PASSWORD} and ${ERROR_MESSAGE}